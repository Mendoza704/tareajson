const bd =[
    {
        "Id":0,
        "Apellidos":"Anchundia",
        "Nombres":"Leonel",
        "Semestre":"Quinto",
        "Paralelo":"A",
        "Direccion":"Montecristi",
        "Telefono":"0987456321",
        "Email":"leonelanchundia@gmail.com"
    },
    {
        "Id":1,
        "Apellidos":"Mendoza",
        "Nombres":"Jonathan",
        "Semestre":"Quinto",
        "Paralelo":"A",
        "Direccion":"Los bajos",
        "Telefono":"0213456789",
        "Email":"jonathan@gmail.com"
    },
    {
        "Id":2,
        "Apellidos":"Quijije",
        "Nombres":"Veronica",
        "Semestre":"Cuarto",
        "Paralelo":"C",
        "Direccion":"Los Bajos",
        "Telefono":"0987456321",
        "Email":"veronica@gmail.com"
    },
    {
        "Id":3,
        "Apellidos":"Moncayo",
        "Nombres":"Andres",
        "Semestre":"Quinto",
        "Paralelo":"A",
        "Direccion":"Manta",
        "Telefono":"0987432561",
        "Email":"andres@gmail.com"
    },
    {
        "Id":4,
        "Apellidos":"Quijije",
        "Nombres":"Evelyn",
        "Semestre":"Sexto",
        "Paralelo":"A",
        "Direccion":"Los bajos",
        "Telefono":"0987412354",
        "Email":"evelyn@gmail.com"
    },
    {
        "Id":5,
        "Apellidos":"Espinoza",
        "Nombres":"Eddy",
        "Semestre":"Quinto",
        "Paralelo":"A",
        "Direccion":"Montecristi",
        "Telefono":"0987456345",
        "Email":"eddy@gmail.com"
    },
    {
        "Id":6,
        "Apellidos":"Mendoza",
        "Nombres":"Lucia",
        "Semestre":"Quinto",
        "Paralelo":"C",
        "Direccion":"Los Bajos",
        "Telefono":"098745632",
        "Email":"lucia@gmail.com"
    },
    {
        "Id":7,
        "Apellidos":"Chiquito",
        "Nombres":"Ivan",
        "Semestre":"Noveno",
        "Paralelo":"A",
        "Direccion":"Manta",
        "Telefono":"0986541223",
        "Email":"ivan@gmail.com"
    },
    {
        "Id":8,
        "Apellidos":"Lucas",
        "Nombres":"Jonathan",
        "Semestre":"Primero",
        "Paralelo":"A",
        "Direccion":"Los Bajos",
        "Telefono":"0987563214",
        "Email":"jonathan@gmail.com"
    },
    {
        "Id":9,
        "Apellidos":"Posligua",
        "Nombres":"Barbara",
        "Semestre":"Tercero",
        "Paralelo":"A",
        "Direccion":"Los bajos",
        "Telefono":"09873564125",
        "Email":"barbara@gmail.com"
    }
]

const estudiantes = document.querySelectorAll('.nombre_estudiante');

estudiantes.forEach((estudiante)=>{
    estudiante.addEventListener('click', (nombre)=>{
        let id=nombre.target.getAttribute('estu-id');
        bd.forEach((estudiante)=>{
            if(id == estudiante.Id){    
                const verDetalle=nombre.target.parentElement.lastElementChild;
                verDetalle.innerHTML=`
                                    <div class="lista">

                                        <div class="nom">
                                        <h2>Apellidos:</h2>
                                        <p>${estudiante.Apellidos}</p>
                                        </div>
                                        <div class="nom">
                                            <h2>Nombre:</h2>
                                            <p>${estudiante.Nombres}</p>
                                        </div>
                                        <div class="nom">
                                            <h2>Semestre:</h2>
                                            <p>${estudiante.Semestre}</p>
                                        </div>
                                        <div class="nom">
                                            <h2>Paralelo:</h2>
                                            <p>${estudiante.Paralelo}</p>
                                        </div>
                                        <div class="nom">
                                            <h2>Direccion:</h2>
                                            <p>${estudiante.Direccion}</p>
                                        </div>
                                        <div class="nom">
                                            <h2>Teléfono:</h2>
                                            <p>${estudiante.Telefono}</p>
                                        </div>
                                        <div class="nom">
                                            <h2>Correo:</h2>
                                            <p>${estudiante.Email}</p>
                                        </div> 
                                    </div>`

            }
        })
    })
})